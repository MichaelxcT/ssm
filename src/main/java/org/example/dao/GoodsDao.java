package org.example.dao;

import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;
import org.example.entity.Goods;

import java.lang.reflect.Type;
import java.util.List;
/////哈哈哈哈哈哈
public interface GoodsDao {
    @Select("select goods.id, goods.`name`,goods.type_id, goods.price, goods.num, goods.produce_time, goods.images, goods.time, type.`name` AS type_name from goods JOIN type on goods.type_id = type.id")
    List<Goods> getAll();

    @Insert("insert into goods values(null,#{name},#{type_id},#{price}, #{num}, #{produce_time},#{images},now())")
    int insert(Goods goods);

    @Delete("DELETE FROM goods WHERE id = #{id}")
    int delete(int id);

    @Update("UPDATE goods SET name = #{name}, type_id = #{type_id}, price = #{price}, num = #{num}, " +
            "produce_time = #{produce_time}, images = #{images} WHERE id = #{id}")
    int update(Goods goods);

    @Select("SELECT DISTINCT type.name AS type_name FROM type JOIN goods ON goods.type_id = type.id ")
    List<String> getAllTypes();



}
