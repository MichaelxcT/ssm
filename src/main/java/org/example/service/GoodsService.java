package org.example.service;
import org.example.entity.Goods;

import java.util.List;

public interface GoodsService {
    List<Goods> getAll();
    List<String> getAllTypes();

    int insert(Goods goods);
    int delete(Integer id);
    void update(Goods goods);

}
