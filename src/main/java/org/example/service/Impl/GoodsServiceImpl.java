package org.example.service.Impl;
import org.example.dao.GoodsDao;
import org.example.entity.Goods;
import org.example.service.GoodsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class GoodsServiceImpl implements GoodsService {


    private GoodsDao goodsDao ;


    @Autowired
    public void setGoodsDao(GoodsDao goodsDao) {
        this.goodsDao = goodsDao;
    }

    @Override
    public List<Goods> getAll() {
        System.out.println("Inside getAll() method");
        return goodsDao.getAll();
    }

    @Override
    public int insert(Goods goods) {
        return goodsDao.insert(goods);
    }
    @Override
    public int delete(Integer id) {
        return goodsDao.delete(id);
    }

    @Override
    public void update(Goods goods) {
        goodsDao.update(goods);
    }
    @Override
    public List<String> getAllTypes() {
        return goodsDao.getAllTypes();
    }




}
