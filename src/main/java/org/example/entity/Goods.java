package org.example.entity;

import java.util.Date;
import java.util.List;

public class Goods {
    private Integer id;
    private String name;
    private Integer type_id;
    private double price;
    private Integer num;
    private Date produce_time;
    private String images;
    private Date time;
    private String type_name;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getType_id() {
        return type_id;
    }

    public void setType_id(Integer type_id) {
        this.type_id = type_id;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public Integer getNum() {
        return num;
    }

    public void setNum(Integer num) {
        this.num = num;
    }

    public Date getProduce_time() {
        return produce_time;
    }

    public void setProduce_time(Date produce_time) {
        this.produce_time = produce_time;
    }

    public String getImages() {
        return images;
    }

    public void setImages(String images) {
        this.images = images;
    }

    public Date getTime() {
        return time;
    }

    public void setTime(Date time) {
        this.time = time;
    }

    public String getType_name() {
        return type_name;
    }

    public void setType_name(String type_name) {
        this.type_name = type_name;
    }

    @Override
    public String toString() {
        return "Goods{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", type_id=" + type_id +
                ", price=" + price +
                ", num=" + num +
                ", produce_time=" + produce_time +
                ", images='" + images + '\'' +
                ", time=" + time +
                ", type_name='" + type_name + '\'' +
                '}';
    }
}
