package org.example.web;

import org.example.entity.Goods;
import org.example.service.GoodsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.ServletContext;
import java.io.File;
import java.util.List;
import java.util.UUID;

@RestController
@RequestMapping("/goods")
@CrossOrigin(origins = "http://127.0.0.1:8848")
public class GoodsController {

    @Autowired
    private ServletContext servletContext;
    private GoodsService goodsService;

    @Autowired
    public GoodsController(GoodsService goodsService) {
        this.goodsService = goodsService;
    }

    @GetMapping("/getAll.do")
    public List<Goods> getAll() {
        return goodsService.getAll();
    }
    @GetMapping("/getAllTypes.do")
    public List<String> getAllTypes() {
        return goodsService.getAllTypes();
    }


    @PostMapping("/save.do")
    public String add(@RequestBody Goods goods) {
        try {
            goodsService.insert(goods);
            return "OK";
        } catch (Exception e) {
            e.printStackTrace();
            return "error";
        }
    }


    @PostMapping("/upload.do")
    public String upload(@RequestParam("file") MultipartFile file, @RequestParam("typeId") int typeId) {
        try {
            // 得到文件名，储存文件时使用
            String filename = file.getOriginalFilename();
            // 得到文件后缀名
            String suffix = filename.substring(filename.lastIndexOf("."));
            // 未防止文件名重复，使用UUID生成一个唯一编码
            filename = UUID.randomUUID().toString().replaceAll("-", "") + suffix;

            // 选择上传路径
            String uploadUrl = servletContext.getRealPath("images");

            // 保存文件的新路径
            String newFile = uploadUrl + "/" + filename;
            // 创建文件
            File f = new File(newFile);
            f.createNewFile();
            // 转存文件
            file.transferTo(f);

            // 构建图片保存路径
            String imagePath = "images/" + filename;

            // 构建 Goods 对象
            Goods newGoods = new Goods();
            newGoods.setType_id(typeId); // Set the type_id
            newGoods.setImages(imagePath);

            // 插入数据库
            goodsService.insert(newGoods);

            // 返回图片访问路径
            return "http://localhost:8080/ssm/" + imagePath;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return "error";
    }

    @DeleteMapping("/delete/{id}.do")
    public String delete(@PathVariable int id) {
        try {
            goodsService.delete(id);
            return "OK";
        } catch (Exception e) {
            e.printStackTrace();
            return "error";
        }
    }

    @PutMapping("/update/{id}.do")
    public String update(@PathVariable int id, @RequestBody Goods goods) {
        try {
            goods.setId(id); // Set the ID for the updated goods
            goodsService.update(goods);
            return "OK";
        } catch (Exception e) {
            e.printStackTrace();
            return "error";
        }
    }

    @Autowired
    public void setGoodsService(GoodsService goodsService) {
        this.goodsService = goodsService;
    }
}
